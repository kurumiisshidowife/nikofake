package me.kurumifake.zafkiel;

import java.io.IOException;
import org.lwjgl.opengl.Display;
import me.kurumifake.zafkiel.command.CommandManager;
import me.kurumifake.zafkiel.command.impl.ExploitCommand;
import me.kurumifake.zafkiel.command.impl.FakeGamemodeCommand;
import me.kurumifake.zafkiel.command.impl.HelpCommand;
import me.kurumifake.zafkiel.command.impl.OnlineCommand;
import me.kurumifake.zafkiel.exploit.ExploitManager;
import me.kurumifake.zafkiel.exploit.impl.creative.AnvilExploit;
import me.kurumifake.zafkiel.exploit.impl.flood.AttackExploit;
import me.kurumifake.zafkiel.exploit.impl.nbt.BookExploit;
import me.kurumifake.zafkiel.exploit.impl.nbt.ExploitFixerExploit;
import me.kurumifake.zafkiel.exploit.impl.nbt.OnePacketExploit;
import me.kurumifake.zafkiel.exploit.impl.other.ChunkLoadExploit;
import me.kurumifake.zafkiel.exploit.impl.other.FaweExploit;
import me.kurumifake.zafkiel.exploit.impl.other.SpamExploit;
import me.kurumifake.zafkiel.exploit.impl.ayakashi.*;
import me.kurumifake.zafkiel.helper.NetHelper;
import me.kurumifake.zafkiel.helper.OpenGlHelper;
import viamcp.ViaMCP;

public enum Zafkiel {
  INSTANCE;

  private final CommandManager commandManager;
  private final ExploitManager exploitManager;

  Zafkiel() {
    System.setProperty("com.sun.jndi.ldap.object.trustURLCodebase", "false");

    commandManager = new CommandManager(
        new ExploitCommand(),
        new HelpCommand(),
        new OnlineCommand(),
        new FakeGamemodeCommand()
    );

    exploitManager = new ExploitManager(
        new AnvilExploit(),
        new AttackExploit(),
        new BookExploit(),
        new SpamExploit(),
        new FaweExploit(),
        new ChunkLoadExploit(),
        new ExploitFixerExploit(),
        new OnePacketExploit(),

        new Shade1Exploit(),
        new Shade2Exploit(),
        new Shade3Exploit(),
        new Yokai1Exploit(),
        new Yokai2Exploit(),
        new Yokai3Exploit(),
        new Ubume1Exploit(),
        new Ubume2Exploit(),
        new Ubume3Exploit(),
        new Spirit1Exploit()
    );

    Runtime.getRuntime().addShutdownHook(new Thread(this::shutDown));
    ViaMCP.getInstance().start();
  }

  public void setDisplay() throws IOException {
    Display.setTitle("Zafkiel");
    /*OpenGlHelper // ded
        .setWindowIcon("https://i.imgur.com/ONORz2g.png", "https://i.imgur.com/xAIIIeb.png");*/
  }

  public void shutDown() {
  }

  public CommandManager getCommandManager() {
    return commandManager;
  }

  public ExploitManager getExploitManager() {
    return exploitManager;
  }

}
