package me.kurumifake.zafkiel.exception;

public class CommandException extends RuntimeException {

  public CommandException(String message) {
    super(message);
  }
}
