### yes i use niko as a base for skidding im bad

---

### Features
- LazyLoading for crashers
- Simple CommandAPI
- Simple ExploitAPI
- GuiInGameHook
- Updated Log4J

---

### Building
- Terminal -> `mvn clean install` in working dir (dir with pom.xml)
- Jar file output:  `target/Zafkiel-1.0-SNAPSHOT.jar`
- Move `Zafkiel-1.0-SNAPSHOT.jar` to `versions/Zafkiel`
- Rename it to `Zafkiel.jar`
- Copy folder `Zafkiel` to `.minecraft/versions`

> JSON File: `versions/Zafkiel/Zafkiel.json`

> Please add libraries to json file (don't create fat jar, we don't want jar that have 40mb size xd)

---

### Credits
- MetaClient (mori0) `Json NBT(extra) from MetaClient, sorry but i don't give you other json nbt ¯\_(ツ)_/¯`
- Niko (narumii) `the base`
- Ayakashi (Noxerek) `yes i skid ayakashi`
---
