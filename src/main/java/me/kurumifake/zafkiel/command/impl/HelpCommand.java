package me.kurumifake.zafkiel.command.impl;

import me.kurumifake.zafkiel.Zafkiel;
import me.kurumifake.zafkiel.command.Command;
import me.kurumifake.zafkiel.command.CommandInfo;
import me.kurumifake.zafkiel.exception.CommandException;
import me.kurumifake.zafkiel.helper.ChatHelper;

@CommandInfo(
    alias = "help"
)
public class HelpCommand extends Command {

  @Override
  public void execute(String... args) throws CommandException {
    if (args.length > 0) {
      ChatHelper.printMessage("\n", false);
      Command command = Zafkiel.INSTANCE.getCommandManager().getCommand(args[0])
          .orElseThrow(
              () -> new CommandException(String.format("Command \"%s\" not found.\n", args[0])));

      ChatHelper
          .printMessage(String.format("&5%s&f: %s\n", command.getAlias(), command.getUsage()));

      /*Zafkiel.INSTANCE.getCommandManager().getCommand(args[0])
          .ifPresentOrElse(command -> ChatHelper.printMessage(
              String.format("&5%s&f: &d%s\n", command.getAlias(), command.getUsage())),
              () -> ChatHelper.printMessage(String.format("Command \"%s\" not found.\n", args[0])));*/
      return;
    }

    ChatHelper.printMessage("\n", false);
    Zafkiel.INSTANCE.getCommandManager().getCommands().stream()
        .filter(command -> !(command instanceof HelpCommand))
        .forEach(command -> ChatHelper.printMessage(
            String.format("&5%s &f- &d%s", command.getAlias(), command.getDescription())));

    ChatHelper.printMessage("\n", false);
  }
}
