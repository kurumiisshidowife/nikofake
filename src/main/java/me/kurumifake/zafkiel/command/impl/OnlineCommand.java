package me.kurumifake.zafkiel.command.impl;

import me.kurumifake.zafkiel.command.Command;
import me.kurumifake.zafkiel.command.CommandInfo;
import me.kurumifake.zafkiel.exception.CommandException;
import me.kurumifake.zafkiel.helper.ChatHelper;
import me.kurumifake.zafkiel.helper.PlayerHelper;

@CommandInfo(
    alias = "online",
    description = ":D",
    usage = ".zonline [method]",
    aliases = {"players", "realplayers"}
)
public class OnlineCommand extends Command {

  @Override
  public void execute(String... args) throws CommandException {
    Type type = args.length > 0 ? Type.valueOf(args[0].toUpperCase()) : Type.PLAYER_DATA;
    int onlinePlayers = -1;

    switch (type) {
      case PLAYER_DATA:
        onlinePlayers = PlayerHelper.getOnlinePlayer().size();
        break;
      case TAB_COMPLETE:
        //TODO: LOL I'M LAZY AS FUK
        break;
    }

    ChatHelper.printMessage("Online players: " + onlinePlayers);
  }

  enum Type {
    TAB_COMPLETE, PLAYER_DATA
  }
}
